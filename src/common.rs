use serde::{Serialize, Deserialize};
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct File {
    pub path: String,
    pub hash: String,
    pub size: u64
}



#[derive(Serialize, Deserialize, Debug)]
pub struct Manifest {
    pub version: u64,
    pub files: Vec<File>
}