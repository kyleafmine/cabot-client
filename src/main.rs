mod pack;
mod sync;
pub mod common;

fn main() {
    let mode = std::env::args().nth(1);
    if !mode.is_none() {
        pack::start(std::env::args().nth(2).expect("Provide a path as third argument when using pack!"))
    } else if mode.is_none() {
        sync::start();
    }
    println!("Exiting in 10 seconds...");
    std::thread::sleep(std::time::Duration::from_secs(10));
}
