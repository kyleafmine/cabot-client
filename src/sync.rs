use super::common::*;
use crypto::digest::Digest;
use std::clone::Clone;
#[derive(PartialEq)]
enum Action {
    Delete,
    Download,
}
struct UpdateTask {
    action: Action,
    file: File,
}

pub const UPDATE_URL: &str = "https://dragonpacks.netlify.app/cabot/manifest.json";
pub const OBJECTS_URL: &str = "https://dragonpacks.netlify.app/cabot/objects";

pub fn start() -> () {
    let minecraft_dir;
    if std::env::consts::OS == "linux" {
        minecraft_dir = dirs::home_dir().unwrap().join(".minecraft");
    } else if std::env::consts::OS == "macos" {
        minecraft_dir = dirs::config_dir().unwrap().join("minecraft");
    } else {
        minecraft_dir = dirs::config_dir().unwrap().join(".minecraft");
    }
    if !minecraft_dir.exists() {
        panic!("bruhv you have to have minecraft installed")
    }
    let manifest: Manifest = serde_json::from_str(
        reqwest::blocking::get(UPDATE_URL)
            .unwrap()
            .text()
            .unwrap()
            .as_str(),
    )
    .unwrap();
    if !std::path::Path::new(
        format!("{}/{}", minecraft_dir.to_str().unwrap(), ".cabot_client").as_str(),
    )
    .exists()
    {
        full_sync(minecraft_dir, manifest);
    } else {
        let installed_version = std::fs::read_to_string(format!(
            "{}/{}",
            minecraft_dir.to_str().unwrap(),
            ".cabot_client"
        ));
        let local_manifest =
            serde_json::from_str::<Manifest>(installed_version.unwrap().as_str()).unwrap();
        if local_manifest.version != manifest.version {
            println!("Not up to date, calculating diffrence...");
            let diff = resolve_update(&local_manifest, &manifest);
            do_update(minecraft_dir.clone(), diff);
            std::fs::write(
                format!("{}/{}", minecraft_dir.to_str().unwrap(), ".cabot_client"),
                serde_json::to_string_pretty(&manifest).unwrap(),
            )
            .unwrap();
        } else {
            println!("nothing to do! local manifest matches remote. delete (.minecraft)/.cabot_manifest to force a full update")
        }
    }
}

fn do_update(dir: std::path::PathBuf, diff: Vec<UpdateTask>) {
    let delete_tasks: Vec<&UpdateTask> = diff
        .iter()
        .filter(|task| task.action == Action::Delete)
        .collect();
    let download_tasks: Vec<&UpdateTask> = diff
        .iter()
        .filter(|task| task.action == Action::Download)
        .collect();
    println!("Need to delete {} items", delete_tasks.len());
    if delete_tasks.len() != 0 {
        let delete_bar = indicatif::ProgressBar::new(delete_tasks.len() as u64);
        delete_bar.tick();
        for e in delete_tasks {
            let r = std::fs::remove_file(format!("{}/{}", dir.to_str().unwrap(), &e.file.path));
            if r.is_err() {
                println!("Error deleting file {}", &e.file.path);
            }
        }
        delete_bar.finish_and_clear();
    }
    let mut download_bytes: u64 = 0;
    for e in &download_tasks {
        download_bytes += e.file.size;
    }
    println!(
        "Need to download {} items ({} bytes)",
        &download_tasks.len(),
        download_bytes
    );
    if download_tasks.len() != 0 {
        let download_bar = indicatif::ProgressBar::new(download_bytes);
        download_bar.tick();
        let client = reqwest::blocking::ClientBuilder::new()
            .timeout(None)
            .build()
            .unwrap();
        for f in &download_tasks {
            let e = client
                .get(format!("{}/{}/{}", OBJECTS_URL, &f.file.hash[0..2], &f.file.hash).as_str())
                .send()
                .unwrap()
                .bytes();
            if e.is_err() {
                println!("Errored downloading {}", &f.file.path);
                continue;
            }
            let response = e.unwrap().to_vec();
            download_bar.inc(f.file.size);
            download_bar.tick();
            std::fs::create_dir_all(
                std::path::Path::new(format!("{}/{}", dir.to_str().unwrap(), &f.file.path).as_str())
                    .parent()
                    .unwrap(),
            )
            .unwrap();
            std::fs::write(format!("{}/{}", dir.to_str().unwrap(), &f.file.path), response).unwrap();
        }
        download_bar.finish_and_clear();
    }
    
}

fn resolve_update(local: &Manifest, remote: &Manifest) -> Vec<UpdateTask> {
    let local_file_map = file_hashmap(&local);
    let remote_file_map = file_hashmap(&remote);
    let mut actions = Vec::new();

    // Checking for deleted files in new version
    for e in &local.files {
        if !remote_file_map.contains_key(&e.hash) {
            // file has been removed in new version
            actions.push(UpdateTask {
                action: Action::Delete,
                file: e.clone(),
            })
        }
    }
    // Checking for new files
    for e in &remote.files {
        if !local_file_map.contains_key(&e.hash) {
            // file has been added in new version
            actions.push(UpdateTask {
                action: Action::Download,
                file: e.clone(),
            })
        }
    }
    return actions;
}

fn file_hashmap(m: &Manifest) -> std::collections::HashMap<String, File> {
    let mut map = std::collections::HashMap::new();
    for e in &m.files {
        map.insert(e.hash.clone(), e.clone());
    }
    return map;
}

fn full_sync(dir: std::path::PathBuf, manifest: Manifest) -> () {
    let mut byte_total: u64 = 0;
    for f in &manifest.files {
        byte_total += f.size;
    }
    println!("Have to download {} megabytes", byte_total / 1000000);
    let bar = indicatif::ProgressBar::new(byte_total);
    bar.tick();
    let client = reqwest::blocking::ClientBuilder::new()
        .timeout(None)
        .build()
        .unwrap();
    for f in &manifest.files {
        bar.set_message(format!("Downloading {}", &f.path).as_str());
        bar.tick();
        let e = client
            .get(format!("{}/{}/{}", OBJECTS_URL, &f.hash[0..2], f.hash).as_str())
            .send()
            .unwrap()
            .bytes();
        if e.is_err() {
            println!("Errored downloading {}", &f.path);
            continue;
        }
        let response = e.unwrap().to_vec();
        bar.inc(f.size);
        bar.tick();
        std::fs::create_dir_all(
            std::path::Path::new(format!("{}/{}", dir.to_str().unwrap(), &f.path).as_str())
                .parent()
                .unwrap(),
        )
        .unwrap();
        std::fs::write(format!("{}/{}", dir.to_str().unwrap(), &f.path), response).unwrap();
    }
    bar.finish_and_clear();
    std::fs::write(
        format!("{}/{}", dir.to_str().unwrap(), ".cabot_client"),
        serde_json::to_string_pretty(&manifest).unwrap(),
    )
    .unwrap();
}
