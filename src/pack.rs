use crypto::digest::Digest;
use super::common::*;
use rand::Rng;

pub fn start(path: String) -> () {
    let file_tree = std::fs::read_dir(path).unwrap();
    let mut spinner = indicatif::ProgressBar::new_spinner();
    println!("{}", std::path::Path::new(&std::env::args().nth(2).unwrap()).canonicalize().unwrap().to_str().unwrap());
    spinner.tick();
    spinner.set_message("Starting...");
    let mut files = Vec::new(); 
    check_output_dir();
    for x in file_tree {
        if (x.is_err()) {
            println!("Warn: Error reading some file");
        }
        let entry = x.unwrap();
        if entry.file_type().unwrap().is_dir() {
            traverse_dir(entry, &mut files, &mut spinner);
        } else {
            add_file(entry, &mut files, &mut spinner);
        }

    }
    spinner.finish_with_message(format!("Finished with {} files read", files.len()).as_str());
    write_manifest(files);
    
}

fn check_output_dir() {
    let base = std::path::Path::new("output");
    if !base.exists() {
        std::fs::create_dir(base).unwrap();
        std::fs::create_dir("output/objects").unwrap();
    }
    
}

fn write_file(f: &File, data: Vec<u8>) {
    let prefix = &f.hash[0..2];
    if !std::path::Path::new(&format!("output/objects/{}", prefix)).exists() {
        std::fs::create_dir(format!("output/objects/{}", prefix)).unwrap();
    }
    std::fs::write(format!("output/objects/{}/{}", prefix, f.hash), data).unwrap();   
}

fn write_manifest(files: Vec<File>) {
    let m = Manifest {
        version: rand::thread_rng().gen(),
        files
    };
    let e = serde_json::to_string(&m);
    std::fs::write("output/manifest.json", e.unwrap()).unwrap();
}


fn traverse_dir(dir: std::fs::DirEntry, files: &mut Vec<File>, spinner: &mut indicatif::ProgressBar) {
    spinner.set_message( format!("Reading directory {}", dir.file_name().to_str().unwrap()).as_str() );
    spinner.tick();
    for e in dir.path().read_dir().unwrap() {
        if e.is_err() {
            println!("wtf");
            continue;
        }
        let entry = e.unwrap();
        if entry.file_type().unwrap().is_dir() {
            traverse_dir(entry, files,  spinner);
        } else {
            add_file(entry, files, spinner);
        }
    }
}


fn add_file(file: std::fs::DirEntry, files: &mut Vec<File>, spinner: &mut indicatif::ProgressBar) {
    let mut hasher = crypto::sha2::Sha256::new();
    
    spinner.set_message( format!("Reading file {}", file.file_name().to_str().unwrap()).as_str() );
    spinner.tick();
    if file.path().is_dir() {
        return traverse_dir(file, files, spinner)
    }
    let data = std::fs::read(file.path()).expect(format!("Error reading {}", file.path().to_str().unwrap()).as_str());
    hasher.input(&data);
    //println!("{}", file.path().to_str().unwrap());
    let f = File {
        path: regex::Regex::new("^[a-zA-Z]*/").unwrap().replace(file.path().to_str().unwrap(), "").to_string(),
        size: data.len() as u64,
        hash: hasher.result_str()
    };
    write_file(&f, data);
    files.push(f)
}